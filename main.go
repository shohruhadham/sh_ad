package main

import (
	"SH_AD/api"
	"SH_AD/app"
	"log"
	"net/http"
	"os"
)

func main() {
	var api = api.New(
		os.Getenv("DB_USER"),
		os.Getenv("DB_PSWD"),
		os.Getenv("DB_ADDR"),
		os.Getenv("DB_NAME"),
		os.Getenv("PSWD_FORM_BASE_LINK"),
	)

	go func() {
		log.Fatalln(http.ListenAndServe(":8001", api))
	}()

	var app = app.New()
	log.Fatalln(http.ListenAndServe(":8000", app))
}
