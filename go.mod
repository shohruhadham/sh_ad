module SH_AD

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/shohruhadham/nanomux v0.0.0-20220219155707-0757689ea701
	golang.org/x/oauth2 v0.0.0-20220223155221-ee480838109b
)

require (
	github.com/golang/protobuf v1.4.2 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
