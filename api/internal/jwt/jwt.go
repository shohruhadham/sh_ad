package jwt

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hash"
	"os"
	"strings"
	"time"
)

// --------------------------------------------------

var (
	header   string
	HS256    hash.Hash
	invalid  = "invalid token %q"
	tampered = "tampered token %q"
)

const (
	aTkDuration = 2 * time.Minute
	rTkDuration = 6 * time.Hour
)

func init() {
	var key = os.Getenv("SH_AD_K")
	if key == "" {
		panic("invalid key")
	}

	header = base64.URLEncoding.EncodeToString(
		[]byte(`{"alg":"HS256","typ":"JWT"}`),
	)

	HS256 = hmac.New(sha256.New, []byte(key))
}

// -------------------------

type _Payload struct {
	User    string `json:"user"`
	TokenId uint64 `json:"tokenId"`
	Exp     int64  `json:"exp"`
}

type Tokens struct {
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

// --------------------------------------------------

func newTokenFor(
	user string,
	tokenId uint64,
	duration time.Duration,
) (string, error) {
	var p = _Payload{
		user,
		tokenId,
		time.Now().Add(duration).Unix(),
	}

	var payloadBytes, err = json.Marshal(&p)
	if err != nil {
		return "", err
	}

	var payload = base64.URLEncoding.EncodeToString(payloadBytes)
	var tokenHead = header + "." + payload
	var signature = base64.URLEncoding.EncodeToString(
		HS256.Sum([]byte(tokenHead)),
	)

	return tokenHead + "." + signature, nil
}

// -------------------------
func NewAccessTokenFor(user string, tokenId uint64) (string, error) {
	return newTokenFor(user, tokenId, aTkDuration)
}

func NewTokensFor(user string, tokenId uint64) (*Tokens, error) {
	var (
		tokens Tokens
		err    error
	)

	tokens.AccessToken, err = newTokenFor(user, tokenId, aTkDuration)
	if err != nil {
		return nil, err
	}

	tokens.RefreshToken, err = newTokenFor(user, tokenId, rTkDuration)
	if err != nil {
		return nil, err
	}

	return &tokens, nil
}

func Parse(token string) (user string, tokenId uint64, exp int64, err error) {
	var idx = strings.LastIndex(token, ".")
	if idx < 0 {
		err = fmt.Errorf(invalid, token)
		return
	}

	var (
		tokenHead         = token[:idx]
		expectedSignature []byte
	)

	expectedSignature, err = base64.URLEncoding.DecodeString(token[idx+1:])
	if err != nil {
		err = fmt.Errorf(invalid, token)
		return
	}

	var signature = HS256.Sum([]byte(tokenHead))
	if !hmac.Equal(signature, expectedSignature) {
		err = fmt.Errorf(tampered, token)
		return
	}

	idx = strings.Index(tokenHead, ".")
	if idx < 0 { // This is a redundant check. But...
		err = fmt.Errorf(invalid, token)
		return
	}

	var payloadBytes []byte
	payloadBytes, err = base64.URLEncoding.DecodeString(tokenHead[idx+1:])
	if err != nil {
		return
	}

	var p _Payload
	err = json.Unmarshal(payloadBytes, &p)
	if err != nil {
		return
	}

	return p.User, p.TokenId, p.Exp, nil
}
