package database

import (
	"os"
	"reflect"
	"testing"
)

// --------------------------------------------------

func TestDb(t *testing.T) {
	var mdb = New(
		os.Getenv("DB_TEST_USER"),
		os.Getenv("DB_TEST_PSWD"),
		os.Getenv("DB_TEST_ADDR"),
		os.Getenv("DB_TEST_NAME"),
	)

	defer func() {
		var _, err = mdb.mysql.Exec(
			"DROP TABLE user_profiles;",
		)

		if err != nil {
			t.Fatalf("couldn't drop test tables")
		}
	}()

	var email = "john.doe@example.com"

	var userProf = &UserProfile{
		Email:       email,
		FullName:    "John Doe",
		Address:     "unknown",
		PhoneNumber: "1234567",
		Password:    "123",
	}

	var err = mdb.InsertUserProfile(userProf)
	if err != nil {
		t.Fatalf("InsertUserProfile: %v", err)
	}

	var gotUserProf *UserProfile
	gotUserProf, err = mdb.UserProfileOf(userProf.Email, false)
	if err != nil {
		t.Fatalf("UserProfileOf: %v", err)
	}

	gotUserProf.Password = userProf.Password
	if !reflect.DeepEqual(gotUserProf, userProf) {
		t.Fatalf(
			"UserProfileOf: userProf = %v, want %v",
			*gotUserProf,
			*userProf,
		)
	}

	userProf.Email = "jane.doe@example.com"
	userProf.FullName = "Jane Doe"
	userProf.Address = "unknown"
	userProf.PhoneNumber = "7654321"

	err = mdb.UpdateUserProfile(email, userProf)
	if err != nil {
		t.Fatalf("UpdateUserProfile: %v", err)
	}

	gotUserProf = nil
	gotUserProf, err = mdb.UserProfileOf(userProf.Email, true)
	if err != nil {
		t.Fatalf("UserProfileOf: %v", err)
	}

	if !reflect.DeepEqual(gotUserProf, userProf) {
		t.Fatalf(
			"UserProfileOf: userProf = %v, want %v",
			*gotUserProf,
			*userProf,
		)
	}

	email = userProf.Email

	var password string
	password, err = mdb.PasswordOf(email)
	if err != nil {
		t.Fatalf("PasswordOf: %v", err)
	}

	if password != userProf.Password {
		t.Fatalf(
			"PasswordOf: password = %v, want %v",
			password,
			userProf.Password,
		)
	}

	userProf.Password = "abc"
	err = mdb.UpdatePassword(email, userProf.Password)
	if err != nil {
		t.Fatalf("UpdatePassword: %v", err)
	}

	password = ""
	password, err = mdb.PasswordOf(email)
	if err != nil {
		t.Fatalf("PasswordOf: %v", err)
	}

	if password != userProf.Password {
		t.Fatalf(
			"PasswordOf: password = %v, want %v",
			password,
			userProf.Password,
		)
	}

	err = mdb.IncrementSessionCount(email)
	if err != nil {
		t.Fatalf("IncrementSessionCount: %v", err)
	}

	var sessionCount uint64
	sessionCount, err = mdb.SessionCountOf(email)
	if err != nil {
		t.Fatalf("SessionCountOf: %v", err)
	}

	if sessionCount != 1 {
		t.Fatalf(
			"SessionCountOf: sessionCount = %v, want 1",
			sessionCount,
		)
	}

	password, sessionCount = "", 0
	password, sessionCount, err = mdb.PasswordAndSessionCountOf(email)
	if err != nil {
		t.Fatalf("PasswordAndSessionCountOf: %v", err)
	}

	if password != userProf.Password {
		t.Fatalf(
			"PasswordAndSessionCountOf: password = %v, want %v",
			password,
			userProf.Password,
		)
	}

	if sessionCount != 1 {
		t.Fatalf(
			"PasswordAndSessionCountOf: sessionCount = %v, want 1",
			sessionCount,
		)
	}
}
