package database

import (
	"database/sql"

	"github.com/go-sql-driver/mysql"
)

// --------------------------------------------------

type UserProfile struct {
	Email       string `json:"email"`
	FullName    string `json:"fullName"`
	Address     string `json:"address"`
	PhoneNumber string `json:"phoneNumber"`

	Password     string `json:"-"`
	SessionCount uint64 `json:"-"`
}

// --------------------------------------------------

type _Statement uint8

const (
	insertUserProfile _Statement = iota
	getUserProfile
	getUserProfileWithPassword
	updateUserProfile
	getPassword
	updatePassword
	getSessionCount
	incrementSessionCount
	getPasswordAndSessionCount

	statementCount
)

// --------------------------------------------------

func createTables(db *sql.DB) error {
	var _, err = db.Exec(
		`CREATE TABLE IF NOT EXISTS user_profiles (
			email VARCHAR(255) PRIMARY KEY,
			full_name VARCHAR(127),
			address VARCHAR(127),
			phone_number VARCHAR(15),
			password VARCHAR(255),
			session_count INT NOT NULL DEFAULT 0
		);`,
	)

	return err
}

func dropTables(db *sql.DB) error {
	var _, err = db.Exec(
		`DROP TABLE IF EXISTS user_profiles;`,
	)

	return err
}

func prepareStatements(db *sql.DB) (stmts []*sql.Stmt, err error) {
	stmts = make([]*sql.Stmt, statementCount)

	stmts[insertUserProfile], err = db.Prepare(
		`INSERT INTO user_profiles (
			email,
			full_name,
			address,
			phone_number,
			password
		) VALUES (?, ?, ?, ?, ?);`,
	)

	if err != nil {
		return nil, err
	}

	stmts[getUserProfile], err = db.Prepare(
		`SELECT email, full_name, address, phone_number, session_count
		FROM user_profiles WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	stmts[getUserProfileWithPassword], err = db.Prepare(
		`SELECT email, full_name, address, phone_number, password, session_count
		FROM user_profiles WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	stmts[updateUserProfile], err = db.Prepare(
		`UPDATE user_profiles
		SET email = ?, full_name = ?, address = ?, phone_number = ?
		WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	stmts[getPassword], err = db.Prepare(
		`SELECT password FROM user_profiles WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	stmts[updatePassword], err = db.Prepare(
		`UPDATE user_profiles
		SET password = ?
		WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	stmts[getSessionCount], err = db.Prepare(
		`SELECT session_count FROM user_profiles WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	stmts[incrementSessionCount], err = db.Prepare(
		`UPDATE user_profiles
		SET session_count = session_count + 1
		WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	stmts[getPasswordAndSessionCount], err = db.Prepare(
		`SELECT password, session_count FROM user_profiles WHERE email = ?;`,
	)

	if err != nil {
		return nil, err
	}

	return stmts, nil
}

// -------------------------

type Db struct {
	mysql *sql.DB
	stmts []*sql.Stmt
}

func New(user, password, addr, dbName string) *Db {
	var conf = mysql.Config{
		User:                 user,
		Passwd:               password,
		Net:                  "tcp",
		Addr:                 addr,
		DBName:               dbName,
		AllowNativePasswords: true,
		ParseTime:            true,
	}

	var db, err = sql.Open("mysql", conf.FormatDSN())
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	err = createTables(db)
	if err != nil {
		panic(err)
	}

	var m = &Db{mysql: db}

	m.stmts, err = prepareStatements(db)
	if err != nil {
		panic(err)
	}

	return m
}

// -------------------------

func (db *Db) InsertUserProfile(userProf *UserProfile) error {
	var _, err = db.stmts[insertUserProfile].Exec(
		userProf.Email,
		userProf.FullName,
		userProf.Address,
		userProf.PhoneNumber,
		userProf.Password,
	)

	return err
}

func (db *Db) UserProfileOf(
	email string,
	withPassword bool,
) (*UserProfile, error) {
	var userProf UserProfile
	var err error

	if withPassword {
		err = db.stmts[getUserProfileWithPassword].QueryRow(email).Scan(
			&userProf.Email,
			&userProf.FullName,
			&userProf.Address,
			&userProf.PhoneNumber,
			&userProf.Password,
			&userProf.SessionCount,
		)
	} else {
		err = db.stmts[getUserProfile].QueryRow(email).Scan(
			&userProf.Email,
			&userProf.FullName,
			&userProf.Address,
			&userProf.PhoneNumber,
			&userProf.SessionCount,
		)
	}

	if err != nil {
		return nil, err
	}

	return &userProf, nil
}

func (db *Db) UpdateUserProfile(
	email string,
	userProf *UserProfile,
) error {
	var _, err = db.stmts[updateUserProfile].Exec(
		userProf.Email,
		userProf.FullName,
		userProf.Address,
		userProf.PhoneNumber,
		email,
	)

	return err
}

func (db *Db) PasswordOf(email string) (string, error) {
	var password string
	var err = db.stmts[getPassword].QueryRow(email).Scan(&password)
	if err != nil {
		return "", err
	}

	return password, nil
}

func (db *Db) UpdatePassword(email, password string) error {
	var _, err = db.stmts[updatePassword].Exec(password, email)
	return err
}

func (db *Db) SessionCountOf(email string) (uint64, error) {
	var sessionCount uint64
	var err = db.stmts[getSessionCount].QueryRow(email).Scan(&sessionCount)
	if err != nil {
		return 0, err
	}

	return sessionCount, nil
}

func (db *Db) IncrementSessionCount(email string) error {
	var _, err = db.stmts[incrementSessionCount].Exec(email)
	return err
}

func (db *Db) PasswordAndSessionCountOf(email string) (string, uint64, error) {
	var password string
	var sessionCount uint64
	var err = db.stmts[getPasswordAndSessionCount].QueryRow(email).Scan(
		&password,
		&sessionCount,
	)

	if err != nil {
		return "", 0, err
	}

	return password, sessionCount, nil
}

// -------------------------

func (db *Db) DropTables() {
	dropTables(db.mysql)
}

func (db *Db) Close() {
	if db == nil {
		return
	}

	for _, stmt := range db.stmts {
		stmt.Close()
	}

	db.mysql.Close()
}
