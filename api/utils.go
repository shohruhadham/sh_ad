package api

import (
	"SH_AD/api/internal/database"
	"SH_AD/api/internal/jwt"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	nm "github.com/shohruhadham/nanomux"
)

// --------------------------------------------------

var gAPIUserInfo = "https://www.googleapis.com/oauth2/v2/userinfo"

// --------------------------------------------------
// JSON structs

type _GUserInfo struct {
	VerifiedEmail bool   `json:"verified_email"`
	Email         string `json:"email"`
	// ID            string `json:"id"`
	// Picture       string `json:"picture"`
}

type _PswdRecoveryResourcesData struct {
	pswdFormBaseLink string
	prdCache         *_PswdRecoveryDataCache
	db               *database.Db
}

type _ValueType uint8

const (
	_string _ValueType = iota
	_bool
)

type _KeyAndType struct {
	key       string
	valueType _ValueType
}

// --------------------------------------------------

// checkIfMIMETypeIsAcceptable checks whether the MIME type of the response can
// be accepted by the client.
func checkIfMIMETypeIsAcceptable(w http.ResponseWriter, r *http.Request) bool {
	var mt = r.Header.Get("Accept")
	if mt != "application/json" && mt != "" {
		return handleErr(w, http.StatusNotAcceptable)
	}

	return false
}

// readRequestBody reads the body and checks whether the given keys and their
// values all exist and returns them. Otherwise, it responds with the proper
// status code and returns nil.
func readRequestBody(w http.ResponseWriter,
	r *http.Request,
	keyAndTypes ..._KeyAndType,
) map[string]interface{} {
	var values map[string]interface{}

	switch r.Header.Get("Content-Type") {
	case "application/json":
		values = make(map[string]interface{})
		var err = json.NewDecoder(r.Body).Decode(&values)
		if err != nil {
			handleErr(w, http.StatusBadRequest)
			return nil
		}

		var failed bool
		for _, kt := range keyAndTypes {
			if v, found := values[kt.key]; found {
				switch kt.valueType {
				case _string:
					if _, ok := v.(string); !ok {
						failed = true
					}

				case _bool:
					if _, ok := v.(bool); !ok {
						failed = true
					}
				}

				if failed {
					handleErr(w, http.StatusBadRequest)
					return nil
				}
			}
		}

		return values

	case "application/x-www-form-urlencoded":
		values = make(map[string]interface{})
		var err = r.ParseForm()
		if err != nil {
			handleErr(w, http.StatusBadRequest)
		}

		for _, kt := range keyAndTypes {
			var v = r.FormValue(kt.key)
			if v == "" {
				handleErr(w, http.StatusBadRequest)
				return nil
			}

			switch kt.valueType {
			case _string:
				values[kt.key] = v

			case _bool:
				if v == "true" {
					values[kt.key] = true
				} else if v == "false" {
					values[kt.key] = false
				} else {
					handleErr(w, http.StatusBadRequest)
					return nil
				}
			}
		}

		return values
	}

	handleErr(w, http.StatusUnsupportedMediaType)
	return nil
}

func writeResponseBody(w http.ResponseWriter, v interface{}) bool {
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(v); err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	return true
}

// -------------------------

func pswdChecksum(password string) string {
	if password == "" {
		return ""
	}

	var chs = sha256.Sum256([]byte(password))
	return base64.URLEncoding.EncodeToString(chs[:])
}

func checkPassword(pswd, pswdChecksum string) bool {
	var chs = sha256.Sum256([]byte(pswd))
	var pchs = base64.URLEncoding.EncodeToString(chs[:])

	return pchs == pswdChecksum
}

// -------------------------

func handleErr(w http.ResponseWriter, status int) bool {
	if status == http.StatusNotAcceptable {
		w.Header().Set("Content-Type", "application/json")
	}

	http.Error(w, http.StatusText(status), status)
	return true
}

func handleUnauthenticated(w http.ResponseWriter) bool {
	http.Error(
		w,
		http.StatusText(http.StatusUnauthorized),
		http.StatusUnauthorized,
	)

	return true
}

// --------------------------------------------------
// Keys of *Args argument.

type _EmailKey struct{}

var emailKey = &_EmailKey{}

type _TokenIdKey struct{}

var tokenIdKey = &_TokenIdKey{}

// --------------------------------------------------
// Middleware

func initialAuthentication(h nm.Handler) nm.Handler {
	return func(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
		var bearer = r.Header.Get("Authorization")
		if bearer == "" {
			return handleUnauthenticated(w)
		}

		var token = strings.TrimPrefix(bearer, "Bearer ")
		if bearer == token {
			return handleUnauthenticated(w)
		}

		var email, tkId, exp, err = jwt.Parse(token)
		if err != nil {
			return handleUnauthenticated(w)
		}

		if exp < time.Now().Unix() {
			return handleUnauthenticated(w)
		}

		args.Set(emailKey, email)
		args.Set(tokenIdKey, tkId)
		return h(w, r, args)
	}
}
