package api

import (
	"sync"
	"time"
)

// --------------------------------------------------

// Password recovery URL duration.
const pswdRecoveryURLDuration = 15 * time.Minute

// -------------------------

type _PasswordRecoveryData struct {
	Token          string
	Email          string
	ExpirationTime time.Time
}

type _PswdRecoveryDataCache struct {
	m       sync.Map
	cleaner *time.Ticker
}

// -------------------------

func newPswdRecoveryDataCache() *_PswdRecoveryDataCache {
	var cache = &_PswdRecoveryDataCache{
		sync.Map{},
		time.NewTicker(pswdRecoveryURLDuration),
	}

	go func() {
		for range cache.cleaner.C {
			var now = time.Now().Unix()
			cache.m.Range(
				func(k, v interface{}) bool {
					var prd = v.(*_PasswordRecoveryData)
					if prd.ExpirationTime.Unix() < now {
						cache.m.Delete(k)
					}

					return true
				},
			)

			var count = 0
			cache.m.Range(
				func(_, _ interface{}) bool {
					count++
					return true
				},
			)

			if count == 0 {
				// If new elements have been added at this moment, they will
				// stay in the cache until used or the new element is added
				// after the stop.
				cache.cleaner.Stop()
			}
		}
	}()

	cache.cleaner.Stop()

	return cache
}

// -------------------------

func (cache *_PswdRecoveryDataCache) keepPasswordRecoveryData(
	prd *_PasswordRecoveryData,
) {
	cache.m.Store(prd.Token, prd)
	cache.cleaner.Reset(pswdRecoveryURLDuration)
}

func (cache *_PswdRecoveryDataCache) passwordRecoveryData(
	token string,
) *_PasswordRecoveryData {
	if prd, found := cache.m.LoadAndDelete(token); found {
		return prd.(*_PasswordRecoveryData)
	}

	return nil
}
