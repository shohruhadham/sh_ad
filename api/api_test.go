package api

import (
	"SH_AD/api/internal/database"
	"SH_AD/api/internal/jwt"
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/shohruhadham/nanomux"
)

// --------------------------------------------------

type _User struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// -------------------------

// 	{ "user1@example.com", "user1pass" }
// 	{ "user2@example.com", "user2pass" }
func testUsers() []_User {
	return []_User{
		{"user1@example.com", ""},
		{"user2@example.com", "user2pass"},
	}
}

// Two registered users:
//
// 	{ "user1@example.com", "user1pass" }
// 	{ "user2@example.com", "user2pass" }
func testAPIResource() (api *nanomux.Resource, cleanDb func()) {
	var db = database.New(
		"tester",
		"password",
		"127.0.0.1:3306",
		"test",
	)

	var tu = testUsers()

	// user1@example.com
	var up = &database.UserProfile{
		Email:       tu[0].Email,
		FullName:    "user 1",
		Address:     "address 1",
		PhoneNumber: "12 345 67 89",

		Password: pswdChecksum(tu[0].Password),
	}

	if err := db.InsertUserProfile(up); err != nil {
		panic(err)
	}

	// user2@example.com
	up = &database.UserProfile{
		Email:       tu[1].Email,
		FullName:    "user 2",
		Address:     "address 2",
		PhoneNumber: "98 765 43 21",

		Password: pswdChecksum(tu[1].Password),
	}

	if err := db.InsertUserProfile(up); err != nil {
		panic(err)
	}

	api = New(
		os.Getenv("DB_TEST_USER"),
		os.Getenv("DB.TEST_PSWD"),
		os.Getenv("DB_TEST_ADDR"),
		os.Getenv("DB_TEST_NAME"),
		os.Getenv("PSWD_FORM_BASE_LINK"),
	)

	return api, func() {
		db.DropTables()
		db.Close()
	}
}

func TestHandleLogin(t *testing.T) {
	var api, cleanDb = testAPIResource()
	defer cleanDb()

	var tu = testUsers()

	var cases = []struct {
		name           string
		user           _User
		wantStatusCode int
	}{
		{
			"non-existent",
			_User{
				Email:    "john.doe@example.com",
				Password: "123",
			},
			http.StatusNotFound,
		},
		{
			tu[0].Email,
			_User{
				Email:    tu[0].Email,
				Password: "123",
			},
			http.StatusUnauthorized, // Signed up with Google.
		},
		{
			tu[1].Email + " wrong password",
			_User{
				Email:    tu[1].Email,
				Password: "...",
			},
			http.StatusUnauthorized,
		},
		{
			tu[1].Email + " empty password",
			_User{
				Email:    tu[1].Email,
				Password: "",
			},
			http.StatusUnauthorized,
		},
		{
			tu[1].Email + " empty email",
			_User{
				Email:    "",
				Password: tu[1].Password,
			},
			http.StatusNotFound,
		},
		{
			tu[0].Email,
			_User{
				Email: tu[0].Email,
			},
			http.StatusUnauthorized,
		},
		{
			tu[1].Email,
			_User{
				Email:    tu[1].Email,
				Password: tu[1].Password,
			},
			http.StatusOK,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var buf = bytes.Buffer{}
			var err = json.NewEncoder(&buf).Encode(&c.user)
			if err != nil {
				panic(err)
			}

			var rec = httptest.NewRecorder()
			var req = httptest.NewRequest("POST", "/api/login", &buf)
			req.Header.Set("Content-Type", "application/json")
			api.ServeHTTP(rec, req)

			if rec.Code != c.wantStatusCode {
				t.Fatalf(
					"invalid status code %d, want %d",
					rec.Code,
					c.wantStatusCode,
				)
			}

			if rec.Code == http.StatusOK {
				var tokens = jwt.Tokens{}
				err = json.Unmarshal(rec.Body.Bytes(), &tokens)
				if err != nil {
					t.Fatal(err)
				}

				var email string
				email, _, _, err = jwt.Parse(tokens.AccessToken)
				if err != nil {
					t.Fatal(err)
				}

				if email != c.user.Email {
					t.Fatalf(
						"invalid access token: email=%q, want %q",
						email,
						c.user.Email,
					)
				}

				email = ""
				email, _, _, err = jwt.Parse(tokens.RefreshToken)
				if err != nil {
					t.Fatal(err)
				}

				if email != c.user.Email {
					t.Fatalf(
						"invalid refresh token: email=%q, want %q",
						email,
						c.user.Email,
					)
				}
			}
		})
	}
}

func TestHandleSignUp(t *testing.T) {
	var api, cleanDb = testAPIResource()
	defer cleanDb()

	var tu = testUsers()

	var cases = []struct {
		name           string
		user           _User
		wantStatusCode int
	}{
		{
			"non-existent",
			_User{
				Email:    "john.doe@example.com",
				Password: "123",
			},
			http.StatusCreated,
		},
		{
			tu[0].Email + " with password",
			_User{
				Email:    tu[0].Email,
				Password: "123",
			},
			http.StatusConflict, // Signed-up with Google.
		},
		{
			tu[0].Email + " without password",
			_User{
				Email:    tu[0].Email,
				Password: "",
			},
			http.StatusConflict,
		},
		{
			tu[1].Email + " with password",
			_User{
				Email:    tu[1].Email,
				Password: tu[1].Password,
			},
			http.StatusConflict,
		},
		{
			tu[1].Email + " without password",
			_User{
				Email:    tu[1].Email,
				Password: "",
			},
			http.StatusConflict,
		},
		{
			"no email",
			_User{
				Password: "123",
			},
			http.StatusBadRequest,
		},
		{
			"invalid email",
			_User{
				Email:    "email.example",
				Password: "123",
			},
			http.StatusBadRequest,
		},
		{
			"no password",
			_User{
				Email: "user3.example.com",
			},
			http.StatusBadRequest,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var buf = bytes.Buffer{}
			var err = json.NewEncoder(&buf).Encode(&c.user)
			if err != nil {
				panic(err)
			}

			var rec = httptest.NewRecorder()
			var req = httptest.NewRequest("POST", "/api/sign-up", &buf)
			req.Header.Set("Content-Type", "application/json")
			api.ServeHTTP(rec, req)

			if rec.Code != c.wantStatusCode {
				t.Fatalf(
					"invalid status code %d, want %d",
					rec.Code,
					c.wantStatusCode,
				)
			}
		})
	}
}

func TestHandleGetUserProfile(t *testing.T) {
	var api, cleanDb = testAPIResource()
	defer cleanDb()

	var tu = testUsers()

	var buf = bytes.Buffer{}
	var err = json.NewEncoder(&buf).Encode(&tu[1])
	if err != nil {
		panic(err)
	}

	var rec = httptest.NewRecorder()
	var req = httptest.NewRequest("POST", "/api/login", &buf)
	req.Header.Set("Content-Type", "application/json")
	api.ServeHTTP(rec, req)

	var tokens jwt.Tokens
	if rec.Code == http.StatusOK {
		err = json.Unmarshal(rec.Body.Bytes(), &tokens)
		if err != nil {
			t.Fatal(err)
		}
	}

	var invalidToken = string(tokens.AccessToken[0]%3) + tokens.AccessToken[1:]

	var cases = []struct {
		name           string
		token          string
		wantStatusCode int
	}{
		{
			tu[1].Email,
			tokens.AccessToken,
			http.StatusOK,
		},
		{
			tu[1].Email + " with invlaid token",
			invalidToken,
			http.StatusUnauthorized,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var rec = httptest.NewRecorder()
			var req = httptest.NewRequest("GET", "/api/user-profile", nil)
			req.Header.Set("Authorization", "Bearer "+c.token)

			api.ServeHTTP(rec, req)

			if rec.Code != c.wantStatusCode {
				t.Fatalf(
					"invalid status code %d, want %d",
					rec.Code,
					c.wantStatusCode,
				)
			}
		})
	}
}

func TestHandlePostUserProfile(t *testing.T) {
	var api, cleanDb = testAPIResource()
	defer cleanDb()

	var tu = testUsers()

	var buf = bytes.Buffer{}
	var err = json.NewEncoder(&buf).Encode(&tu[1])
	if err != nil {
		panic(err)
	}

	var rec = httptest.NewRecorder()
	var req = httptest.NewRequest("POST", "/api/login", &buf)
	req.Header.Set("Content-Type", "application/json")
	api.ServeHTTP(rec, req)

	var tokens jwt.Tokens
	if rec.Code == http.StatusOK {
		err = json.Unmarshal(rec.Body.Bytes(), &tokens)
		if err != nil {
			t.Fatal(err)
		}
	}

	var invalidToken = string(tokens.AccessToken[0]%3) + tokens.AccessToken[1:]

	var cases = []struct {
		name           string
		token          string
		userProf       database.UserProfile
		wantStatusCode int
	}{
		{
			tu[1].Email,
			tokens.AccessToken,
			database.UserProfile{
				Email:       "user3@example.com",
				FullName:    "user 3",
				Address:     "address 3",
				PhoneNumber: "111 11 11",
			},
			http.StatusOK,
		},
		{
			tu[1].Email + " with invlaid token",
			invalidToken,
			database.UserProfile{
				Email:       "user4@example.com",
				FullName:    "user 4",
				Address:     "address 4",
				PhoneNumber: "222 22 22",
			},
			http.StatusUnauthorized,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			buf = bytes.Buffer{}
			var err = json.NewEncoder(&buf).Encode(&c.userProf)
			if err != nil {
				panic(err)
			}

			var rec = httptest.NewRecorder()
			var req = httptest.NewRequest("POST", "/api/user-profile", &buf)
			req.Header.Set("Authorization", "Bearer "+c.token)
			req.Header.Set("Content-Type", "application/json")

			api.ServeHTTP(rec, req)

			if rec.Code != c.wantStatusCode {
				t.Fatalf(
					"invalid status code %d, want %d",
					rec.Code,
					c.wantStatusCode,
				)
			}

			if rec.Code == http.StatusOK {
				var receivedTokens jwt.Tokens
				err = json.Unmarshal(rec.Body.Bytes(), &receivedTokens)
				if err != nil {
					t.Fatal(err)
				}

				if receivedTokens.AccessToken == tokens.AccessToken {
					t.Fatal(
						"handlePostUserProfile has failed to return a new access token",
					)
				}

				if receivedTokens.RefreshToken == tokens.RefreshToken {
					t.Fatal(
						"handlePostUserProfile has failed to return a new refresh token",
					)
				}

				rec = httptest.NewRecorder()
				req = httptest.NewRequest("GET", "/api/user-profile", nil)
				req.Header.Set(
					"Authorization", "Bearer "+receivedTokens.AccessToken,
				)

				req.Header.Set("Content-Type", "application/json")

				api.ServeHTTP(rec, req)

				var userProf database.UserProfile
				err = json.NewDecoder(rec.Body).Decode(&userProf)
				if err != nil {
					t.Fatal(err)
				}

				c.userProf.Password = userProf.Password
				if userProf != c.userProf {
					t.Fatalf("handlePostUserProfile has failed")
				}
			}
		})
	}
}
