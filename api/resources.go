package api

import (
	"SH_AD/api/internal/database"
	"SH_AD/api/internal/jwt"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"net/mail"
	"strconv"
	"time"

	nm "github.com/shohruhadham/nanomux"
)

// --------------------------------------------------
// Resources

// api/login
func handleLogin(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var db = args.ResponderSharedData().(*database.Db)

	if checkIfMIMETypeIsAcceptable(w, r) {
		// The client has already been responded.
		return true
	}

	var values = readRequestBody(
		w, r,
		_KeyAndType{"email", _string}, _KeyAndType{"password", _string},
	)

	if values == nil {
		// The client has already been responded.
		return true
	}

	var email = values["email"].(string)
	var password, sessionCount, err = db.PasswordAndSessionCountOf(email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return handleErr(w, http.StatusNotFound)
		}

		return handleErr(w, http.StatusInternalServerError)
	}

	if !checkPassword(values["password"].(string), password) {
		return handleErr(w, http.StatusUnauthorized)
	}

	var tokens *jwt.Tokens
	tokens, err = jwt.NewTokensFor(email, sessionCount)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	return writeResponseBody(w, tokens)
}

// api/glogin
func handleGLogin(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
	var db = args.ResponderSharedData().(*database.Db)

	if checkIfMIMETypeIsAcceptable(w, r) {
		// The client has already been responded.
		return true
	}

	// The token type and expiration time have not been used. We just need
	// to authenticate the user at login. Then we generate our own tokens.
	var values = readRequestBody(
		w, r,
		_KeyAndType{"accessToken", _string},
		_KeyAndType{"refreshToken", _string},
	)

	if values == nil {
		// The client has already been responded.
		return true
	}

	var req, err = http.NewRequest("GET", gAPIUserInfo, nil)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	req.Header.Set("Authorization", "Bearer "+values["accessToken"].(string))

	var res *http.Response
	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return handleUnauthenticated(w)
	}

	var gUserInfo _GUserInfo
	err = json.NewDecoder(res.Body).Decode(&gUserInfo)
	if err != nil {
		return handleUnauthenticated(w)
	}

	if !gUserInfo.VerifiedEmail {
		return handleUnauthenticated(w)
	}

	var sessionCount uint64
	sessionCount, err = db.SessionCountOf(gUserInfo.Email)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			var userProf = &database.UserProfile{
				Email: gUserInfo.Email,
			}

			db.InsertUserProfile(userProf)
		} else {
			return handleErr(w, http.StatusInternalServerError)
		}
	}

	var tokens *jwt.Tokens
	tokens, err = jwt.NewTokensFor(gUserInfo.Email, sessionCount)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	return writeResponseBody(w, tokens)
}

// -------------------------

func handleRenewal(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
	var db = args.ResponderSharedData().(*database.Db)

	if checkIfMIMETypeIsAcceptable(w, r) {
		// The client has already been responded.
		return true
	}

	var value = readRequestBody(w, r, _KeyAndType{"refreshToken", _string})
	if value == nil {
		// The client has already been responded.
		return true
	}

	var refreshToken = value["refreshToken"].(string)
	var email, tkId, exp, err = jwt.Parse(refreshToken)
	if err != nil {
		return handleUnauthenticated(w)
	}

	var now = time.Now().Unix()
	if exp < now {
		return handleUnauthenticated(w)
	}

	var sessionCount uint64
	sessionCount, err = db.SessionCountOf(email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return handleUnauthenticated(w)
		}

		return handleErr(w, http.StatusInternalServerError)
	}

	if tkId < sessionCount {
		return handleUnauthenticated(w)
	}

	// The refresh token is updated if there are less than 30 minutes until
	// it expires.
	if ((exp - now) / 60) < 30 {
		var tokens *jwt.Tokens
		tokens, err = jwt.NewTokensFor(email, sessionCount)
		if err != nil {
			return handleErr(w, http.StatusInternalServerError)
		}

		return writeResponseBody(w, tokens)
	}

	var token string
	token, err = jwt.NewAccessTokenFor(email, tkId)
	if err != nil {
		handleErr(w, http.StatusInternalServerError)
	}

	var accessToken = struct {
		AccessToken string `json:"accessToken"`
	}{
		token,
	}

	return writeResponseBody(w, &accessToken)
}

// -------------------------

// /api/logout
func handleLogout(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var db = args.ResponderSharedData().(*database.Db)
	var email = args.Get(emailKey).(string)
	var tkId = args.Get(tokenIdKey).(uint64)

	var sessionCount, err = db.SessionCountOf(email)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return handleErr(w, http.StatusInternalServerError)
		}
	} else {
		if tkId < sessionCount {
			// This is an old token. There may be an existing session.
			// So we don't invalidate new tokens.
			return handleUnauthenticated(w)
		}

		err = db.IncrementSessionCount(email)
		if err != nil {
			return handleErr(w, http.StatusInternalServerError)
		}
	}

	w.WriteHeader(http.StatusOK)
	return true
}

// -------------------------

// /api/sign-up
func handleSignUp(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var db = args.ResponderSharedData().(*database.Db)

	if checkIfMIMETypeIsAcceptable(w, r) {
		// The client has already been responded.
		return true
	}

	var values = readRequestBody(
		w, r,
		_KeyAndType{"email", _string},
		_KeyAndType{"password", _string},
	)

	if values == nil {
		// The client has already been responded.
		return true
	}

	var email = values["email"].(string)
	var _, err = mail.ParseAddress(email)
	if err != nil {
		return handleErr(w, http.StatusBadRequest)
	}

	if _, err = db.SessionCountOf(email); err == nil {
		// There is an existing user with the same email address.
		return handleErr(w, http.StatusConflict)
	}

	var pswd = values["password"].(string)
	if pswd == "" {
		return handleErr(w, http.StatusForbidden)
	}

	var userProf = &database.UserProfile{
		Email:    email,
		Password: pswdChecksum(pswd),
	}

	err = db.InsertUserProfile(userProf)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusCreated)

	var tokens *jwt.Tokens
	tokens, err = jwt.NewTokensFor(email, 0)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	return writeResponseBody(w, tokens)
}

// -------------------------

// /api/user-profile
func handleGetUserProfile(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var db = args.ResponderSharedData().(*database.Db)
	var email = args.Get(emailKey).(string)
	var tkId = args.Get(tokenIdKey).(uint64)

	if checkIfMIMETypeIsAcceptable(w, r) {
		// The client has already been responded.
		return true
	}

	var userProf, err = db.UserProfileOf(email, false)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return handleUnauthenticated(w)
		}

		return handleErr(w, http.StatusInternalServerError)
	}

	if tkId < userProf.SessionCount {
		return handleUnauthenticated(w)
	}

	w.Header().Set(
		"Cache-Control",
		"no-store, max-age=0, must-revalidate, private, no-cache",
	)

	return writeResponseBody(w, userProf)
}

// /api/user-profile
func handlePostUserProfile(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var db = args.ResponderSharedData().(*database.Db)
	var email = args.Get(emailKey).(string)
	var tkId = args.Get(tokenIdKey).(uint64)

	if checkIfMIMETypeIsAcceptable(w, r) {
		// The client has already been responded.
		return true
	}

	var values = readRequestBody(
		w, r,
		_KeyAndType{"email", _string},
		_KeyAndType{"fullName", _string},
		_KeyAndType{"address", _string},
		_KeyAndType{"phoneName", _string},
	)

	if values == nil {
		// The client has already been responded.
		return true
	}

	var userProf, err = db.UserProfileOf(email, true)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return handleUnauthenticated(w)
		}

		return handleErr(w, http.StatusInternalServerError)
	}

	if tkId < userProf.SessionCount {
		return handleUnauthenticated(w)
	}

	if userProf.Password != "" {
		// If the user has signed up with a login and password, changing
		// the email address is allowed.

		var profileEmail = values["email"].(string)
		if profileEmail == "" {
			return handleErr(w, http.StatusForbidden)
		}

		userProf.Email = profileEmail
	}

	userProf.FullName = values["fullName"].(string)
	userProf.Address = values["address"].(string)
	userProf.PhoneNumber = values["phoneNumber"].(string)

	err = db.UpdateUserProfile(email, userProf)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	if email != userProf.Email {
		var tokens *jwt.Tokens
		tokens, err = jwt.NewTokensFor(
			userProf.Email,
			userProf.SessionCount,
		)

		if err != nil {
			return handleErr(w, http.StatusInternalServerError)
		}

		return writeResponseBody(w, tokens)
	}

	w.WriteHeader(http.StatusNoContent)
	return true
}

// -------------------------

// /api/pswd-recovery
func handlePasswordRecovery(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var shd = args.ResponderSharedData().(*_PswdRecoveryResourcesData)

	var value = readRequestBody(w, r, _KeyAndType{"email", _string})

	// nil map returns empty string.
	var email = value["email"].(string)
	var _, err = mail.ParseAddress(email)
	if err != nil {
		return handleErr(w, http.StatusBadRequest)
	}

	var password string
	password, err = shd.db.PasswordOf(email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return handleErr(w, http.StatusNotFound)
		}

		return handleErr(w, http.StatusInternalServerError)
	}

	if password == "" {
		return handleErr(w, http.StatusNotFound)
	}

	var (
		expTime    = time.Now().Add(pswdRecoveryURLDuration)
		expTimeStr = strconv.FormatInt(expTime.Unix(), 10)
		tokenBytes = jwt.HS256.Sum([]byte(expTimeStr + "+" + email))

		token = base64.URLEncoding.EncodeToString(tokenBytes)
		url   = shd.pswdFormBaseLink + token
	)

	shd.prdCache.keepPasswordRecoveryData(
		&_PasswordRecoveryData{
			Token:          token,
			Email:          email,
			ExpirationTime: expTime,
		},
	)

	err = sendEmailTo(email, url)
	if err != nil {
		return handleErr(w, http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	return true
}

// /api/pswd-recovery/new
func handleNewPassword(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var shd = args.ResponderSharedData().(*_PswdRecoveryResourcesData)

	var values = readRequestBody(
		w, r,
		_KeyAndType{"token", _string},
		_KeyAndType{"password", _string},
	)

	if values == nil {
		// The client has already been responded.
		return true
	}

	var prData = shd.prdCache.passwordRecoveryData(values["token"].(string))
	if prData == nil {
		return handleErr(w, http.StatusUnauthorized)
	}

	var password, err = shd.db.PasswordOf(prData.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return handleErr(w, http.StatusNotFound)
		}

		return handleErr(w, http.StatusInternalServerError)
	}

	if password == "" {
		return handleErr(w, http.StatusNotFound)
	}

	if prData.ExpirationTime.Unix() < time.Now().Unix() {
		return handleErr(w, http.StatusUnauthorized)
	}

	var pswd = values["password"].(string)
	if pswd == "" {
		return handleErr(w, http.StatusForbidden)
	}

	shd.db.UpdatePassword(prData.Email, pswdChecksum(pswd))

	w.WriteHeader(http.StatusOK)
	return true
}
