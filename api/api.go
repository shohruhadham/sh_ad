package api

import (
	"SH_AD/api/internal/database"

	nm "github.com/shohruhadham/nanomux"
)

// --------------------------------------------------

func New(
	dbUser, dbPassword, dbAddress, dbName string,
	pswdFormBaseLink string,
) *nm.Resource {
	if pswdFormBaseLink == "" {
		panic("empty password form base link")
	}

	if pswdFormBaseLink[len(pswdFormBaseLink)-1] != '/' {
		pswdFormBaseLink += "/"
	}

	var api = nm.NewDormantResource("api/")

	// api/login
	api.SetPathHandlerFor("POST", "login", handleLogin)
	api.SetPathHandlerFor("POST", "glogin/", handleGLogin)

	// api/renewal
	api.SetPathHandlerFor("POST", "renewal", handleRenewal)

	// api/logout
	api.SetPathHandlerFor("GET", "logout", handleLogout)
	api.WrapPathHandlerOf("GET", "logout", initialAuthentication)

	// api/sign-up
	api.SetPathHandlerFor("POST", "sign-up", handleSignUp)

	// api/user-profile
	var userProfile = api.Resource("user-profile")
	userProfile.SetHandlerFor("GET", handleGetUserProfile)
	userProfile.SetHandlerFor("POST", handlePostUserProfile)
	userProfile.WrapRequestHandler(initialAuthentication)

	// api/pswd-recovery
	api.SetPathHandlerFor("POST", "pswd-recovery/", handlePasswordRecovery)
	api.SetPathHandlerFor(
		"POST",
		"pswd-recovery/new",
		handleNewPassword,
	)

	var db = database.New(dbUser, dbPassword, dbAddress, dbName)

	api.SetSharedDataForSubtree(db)

	var prrData = &_PswdRecoveryResourcesData{
		pswdFormBaseLink,
		newPswdRecoveryDataCache(),
		db,
	}

	// A single resource's shared data must be set after setting shared
	// data for the subtree. Otherwise, it gets reset.
	api.SetSharedDataAt("pswd-recovery/", prrData)
	api.SetSharedDataAt("pswd-recovery/new", prrData)

	return api
}
