package api

import (
	"net/smtp"
	"os"
)

func sendEmailTo(email, pswdResetURL string) error {
	var from = os.Getenv("MAIL_FROM")
	var user = os.Getenv("MAIL_USER")
	var pswd = os.Getenv("MAIL_PSWD")
	var toEmail = email
	var to = []string{toEmail}

	var host = os.Getenv("MAIL_HOST")
	var port = "587"
	var addr = host + ":" + port

	var message = []byte(
		"From: " + from + "\r\n" +
			"To: " + to[0] + "\r\n" +
			"Subject: Password Recovery\r\n" +
			"The link:\r\n" +
			pswdResetURL + "\r\n",
	)

	var auth = smtp.PlainAuth("", user, pswd, host)
	var err = smtp.SendMail(addr, auth, from, to, message)
	if err != nil {
		return err
	}

	return nil
}
