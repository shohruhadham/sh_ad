package app

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	nm "github.com/shohruhadham/nanomux"
	"golang.org/x/oauth2"
)

// --------------------------------------------------
// Resources

// login

func getLoginPage(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
	return render(w, r, "login", nil)
}

func handleLogin(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
	var user = _User{
		Email:    r.PostFormValue("email"),
		Password: r.PostFormValue("password"),
	}

	var buf = bytes.Buffer{}
	var err = json.NewEncoder(&buf).Encode(&user)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var res *http.Response
	res, err = http.Post(
		"http://localhost:8001/api/login",
		"application/json",
		&buf,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusBadRequest {
		return render(w, r, "login", "Please enter a valid email address!")
	}

	if res.StatusCode == http.StatusNotFound {
		return render(
			w, r,
			"login",
			"The email address wasn't registered. Please sign up or log in with Google!",
		)
	}

	if res.StatusCode == http.StatusUnauthorized {
		return render(w, r, "login", "Password is invalid!")
	}

	if res.StatusCode != http.StatusOK {
		return handleErr(w, r, res.StatusCode)
	}

	var tokens _Tokens
	err = json.NewDecoder(res.Body).Decode(&tokens)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var cookie = &http.Cookie{
		Name:     "tk",
		Value:    tokens.AccessToken,
		HttpOnly: true,
	}

	http.SetCookie(w, cookie)

	cookie = &http.Cookie{
		Name:     "rtk",
		Value:    tokens.RefreshToken,
		Path:     "/renewal",
		HttpOnly: true,
	}

	http.SetCookie(w, cookie)
	http.Redirect(w, r, "/profile-view", http.StatusSeeOther)

	return true
}

// -------------------------

// /api/glogin/
func handleGLogin(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var url = gOauth2Config.AuthCodeURL(gOauth2State, oauth2.AccessTypeOffline)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
	return true
}

// /api/glogin/user
func handleGLoginUser(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	if r.FormValue("state") != gOauth2State {
		return handleUnauthenticated(w, r)
	}

	var token, err = gOauth2Config.Exchange(
		context.Background(),
		r.FormValue("code"),
	)

	if err != nil {
		return handleUnauthenticated(w, r)
	}

	var gToken = _GToken{
		AccessToken:  token.AccessToken,
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
		ExpTime:      token.Expiry,
	}

	var buf bytes.Buffer
	err = json.NewEncoder(&buf).Encode(&gToken)
	if err != nil {
		handleErr(w, r, http.StatusInternalServerError)
	}

	var res *http.Response
	res, err = http.Post(
		"http://localhost:8001/api/glogin",
		"application/json",
		&buf,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return handleErr(w, r, res.StatusCode)
	}

	var tokens _Tokens
	err = json.NewDecoder(res.Body).Decode(&tokens)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	// The token ID helps to differentiate sign-up from login.
	var idx = strings.Index(tokens.AccessToken, ".")
	if idx < -1 {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var lIdx = strings.LastIndex(tokens.AccessToken, ".")
	if lIdx < -1 {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	if !(idx < lIdx) {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var payloadBytes []byte
	payloadBytes, err = base64.URLEncoding.DecodeString(
		tokens.AccessToken[idx+1 : lIdx],
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var payload = struct {
		TokenId uint64 `json:"tokenId"`
	}{}

	err = json.Unmarshal(payloadBytes, &payload)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var nextPath string
	if payload.TokenId == 0 {
		nextPath = "/profile-form?id="
	} else {
		nextPath = "/profile-view?id="
	}

	var id = strconv.FormatInt(
		time.Now().Unix()+int64(token.AccessToken[0]),
		10,
	)

	tokenMap.Store(id, &tokens)

	w.Header().Set(
		"Cache-Control",
		"no-store, max-age=0, must-revalidate, private, no-cache",
	)

	http.Redirect(w, r, nextPath+id, http.StatusSeeOther)

	return true
}

// -------------------------

// handleRenewal returns an error only if there is an internal error.
// Otherwise, it refreshes the tokens and returns true, or false if the user
// was logged out.
func handleRenewal(w http.ResponseWriter, r *http.Request, _ *nm.Args) bool {
	var cookie, err = r.Cookie("nextHop")
	if err != nil {
		return handleUnauthenticated(w, r)
	}

	var nextHop = cookie.Value

	cookie, err = r.Cookie("rtk")
	if err != nil {
		return handleUnauthenticated(w, r)
	}

	var refreshToken = struct {
		RefreshToken string `json:"refreshToken"`
	}{
		cookie.Value,
	}

	var buf bytes.Buffer
	err = json.NewEncoder(&buf).Encode(&refreshToken)
	if err != nil {
		return handleUnauthenticated(w, r)
	}

	var res *http.Response
	res, err = http.Post(
		"http://localhost:8001/api/renewal",
		"application/json",
		&buf,
	)

	if err != nil {
		return handleUnauthenticated(w, r)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return handleUnauthenticated(w, r)
	}

	var tokens _Tokens
	err = json.NewDecoder(res.Body).Decode(&tokens)
	if err != nil {
		return handleUnauthenticated(w, r)
	}

	cookie = &http.Cookie{
		Name:     "tk",
		Value:    tokens.AccessToken,
		HttpOnly: true,
	}

	http.SetCookie(w, cookie)

	if tokens.RefreshToken != "" {
		cookie = &http.Cookie{
			Name:     "rtk",
			Value:    tokens.RefreshToken,
			Path:     "/renewal",
			HttpOnly: true,
		}

		http.SetCookie(w, cookie)
	}

	// After successful renewal of tokens, we redirect the request to
	// its original link.

	switch nextHop {
	case "profileForm":
		nextHop = "profile-form"
	case "profileView":
		nextHop = "profile-view"
	}

	http.Redirect(w, r, nextHop, http.StatusTemporaryRedirect)
	return true
}

// -------------------------

func handleLogout(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
	var req, err = http.NewRequest(
		"GET",
		"http://localhost:8001/api/logout",
		nil,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var cookie *http.Cookie
	cookie, err = r.Cookie("tk")
	if err != nil {
		return handleErr(w, r, http.StatusUnauthorized)
	}

	req.Header.Set("Authorization", "Bearer "+cookie.Value)

	var res *http.Response
	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	res.Body.Close()
	return handleUnauthenticated(w, r)
}

// -------------------------

// sign-up

func getSignUpPage(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
	return render(w, r, "sign-up", nil)
}

func handleSignUp(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
	var user = _User{
		Email:    r.PostFormValue("email"),
		Password: r.PostFormValue("password"),
	}

	var buf = bytes.Buffer{}
	var err = json.NewEncoder(&buf).Encode(&user)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var res *http.Response
	res, err = http.Post(
		"http://localhost:8001/api/sign-up",
		"application/json",
		&buf,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusBadRequest {
		return render(w, r, "sign-up", "Please enter a valid email and password!")
	}

	if res.StatusCode == http.StatusConflict {
		return render(w, r, "sign-up", "The user already exists!")
	}

	if res.StatusCode != http.StatusCreated {
		return handleErr(w, r, res.StatusCode)
	}

	var tokens _Tokens
	err = json.NewDecoder(res.Body).Decode(&tokens)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var cookie = &http.Cookie{
		Name:     "tk",
		Value:    tokens.AccessToken,
		HttpOnly: true,
	}

	http.SetCookie(w, cookie)

	cookie = &http.Cookie{
		Name:     "rtk",
		Value:    tokens.RefreshToken,
		Path:     "/renewal",
		HttpOnly: true,
	}

	http.SetCookie(w, cookie)

	http.Redirect(w, r, "/profile-form", http.StatusSeeOther)
	return true
}

// -------------------------
// profil-form

func getProfileFormPage(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var cookie *http.Cookie
	if id := r.URL.Query().Get("id"); id != "" {
		if tks, ok := tokenMap.LoadAndDelete(id); ok {
			if tokens, ok := tks.(*_Tokens); ok {
				cookie = &http.Cookie{
					Name:     "tk",
					Value:    tokens.AccessToken,
					HttpOnly: true,
				}

				http.SetCookie(w, cookie)

				cookie = &http.Cookie{
					Name:     "rtk",
					Value:    tokens.RefreshToken,
					Path:     "/renewal",
					HttpOnly: true,
				}

				http.SetCookie(w, cookie)
			}
		}
	} else {
		cookie, _ = r.Cookie("tk")
	}

	if cookie == nil {
		return handleUnauthenticated(w, r)
	}

	var req, err = http.NewRequest(
		"GET",
		"http://localhost:8001/api/user-profile",
		nil,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	req.Header.Set("Authorization", "Bearer "+cookie.Value)

	var res *http.Response
	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusUnauthorized {
		cookie = &http.Cookie{
			Name:     "nextHop",
			Value:    "profileForm",
			Path:     "/renewal",
			HttpOnly: true,
		}

		http.SetCookie(w, cookie)
		http.Redirect(w, r, "/renewal", http.StatusTemporaryRedirect)
		return true
	}

	if res.StatusCode != http.StatusOK {
		return handleErr(w, r, res.StatusCode)
	}

	var userProf _UserProfile
	err = json.NewDecoder(res.Body).Decode(&userProf)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	return render(w, r, "profile-form", &userProf)
}

func handlePostProfileForm(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var userProf = _UserProfile{
		Email:       r.PostFormValue("email"),
		FullName:    r.PostFormValue("full-name"),
		Address:     r.PostFormValue("address"),
		PhoneNumber: r.PostFormValue("phone-number"),
	}

	var buf = bytes.Buffer{}
	var err = json.NewEncoder(&buf).Encode(&userProf)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var req *http.Request
	req, err = http.NewRequest(
		"POST",
		"http://localhost:8001/api/user-profile",
		&buf,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	var cookie *http.Cookie
	cookie, err = r.Cookie("tk")
	if err != nil {
		return handleUnauthenticated(w, r)
	}

	req.Header.Set("Authorization", "Bearer "+cookie.Value)
	req.Header.Set("Content-Type", "application/json")

	var res *http.Response
	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusUnauthorized {
		cookie = &http.Cookie{
			Name:     "nextHop",
			Value:    "profileForm",
			Path:     "/renewal",
			HttpOnly: true,
		}

		http.SetCookie(w, cookie)
		http.Redirect(w, r, "/renewal", http.StatusTemporaryRedirect)
		return true
	}

	if res.StatusCode == http.StatusForbidden {
		userProf.Warning = "The email address must not be empty!"
		return render(w, r, "profile-form", &userProf)
	}

	if res.StatusCode != http.StatusOK &&
		res.StatusCode != http.StatusNoContent {
		return handleErr(w, r, res.StatusCode)
	}

	if res.StatusCode == http.StatusOK {
		var tokens _Tokens
		err = json.NewDecoder(res.Body).Decode(&tokens)
		if err != nil {
			return handleErr(w, r, http.StatusInternalServerError)
		}

		cookie = &http.Cookie{
			Name:     "tk",
			Value:    tokens.AccessToken,
			HttpOnly: true,
		}

		http.SetCookie(w, cookie)

		cookie = &http.Cookie{
			Name:     "tk",
			Value:    tokens.AccessToken,
			Path:     "/renewal",
			HttpOnly: true,
		}

		http.SetCookie(w, cookie)
	}

	http.Redirect(w, r, "/profile-view", http.StatusSeeOther)
	return true
}

// -------------------------
// profil-view

func getProfileViewPage(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var cookie *http.Cookie
	if id := r.URL.Query().Get("id"); id != "" {
		if tks, ok := tokenMap.LoadAndDelete(id); ok {
			if tokens, ok := tks.(*_Tokens); ok {
				cookie = &http.Cookie{
					Name:     "tk",
					Value:    tokens.AccessToken,
					HttpOnly: true,
				}

				http.SetCookie(w, cookie)

				cookie = &http.Cookie{
					Name:     "rtk",
					Value:    tokens.RefreshToken,
					Path:     "/renewal",
					HttpOnly: true,
				}

				http.SetCookie(w, cookie)
			}
		}
	} else {
		cookie, _ = r.Cookie("tk")
	}

	if cookie == nil {
		return handleUnauthenticated(w, r)
	}

	var req, err = http.NewRequest(
		"GET",
		"http://localhost:8001/api/user-profile",
		nil,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	req.Header.Set("Authorization", "Bearer "+cookie.Value)

	var res *http.Response
	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusUnauthorized {
		cookie = &http.Cookie{
			Name:     "nextHop",
			Value:    "profileView",
			Path:     "/renewal",
			HttpOnly: true,
		}

		http.SetCookie(w, cookie)
		http.Redirect(w, r, "/renewal", http.StatusTemporaryRedirect)
		return true
	}

	if res.StatusCode != http.StatusOK {
		return handleErr(w, r, res.StatusCode)
	}

	var userProf _UserProfile
	err = json.NewDecoder(res.Body).Decode(&userProf)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	return render(w, r, "profile-view", &userProf)
}

// -------------------------
// pswd-recovery

func getPswdRecoveryPage(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	return render(w, r, "pswd-recovery", nil)
}

func handlePswdRecoveryEmail(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var email = struct {
		Email string `json:"email"`
	}{
		r.FormValue("email"),
	}

	if email.Email == "" {
		return render(
			w, r,
			"pswd-recovery",
			"Please enter the email address you have signed up with!",
		)
	}

	var buf bytes.Buffer
	var err = json.NewEncoder(&buf).Encode(&email)
	if err != nil {
		handleErr(w, r, http.StatusInternalServerError)
	}

	var req *http.Request
	req, err = http.NewRequest(
		"POST",
		"http://localhost:8001/api/pswd-recovery/",
		&buf,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	req.Header.Set("Content-Type", "application/json")

	var res *http.Response
	res, err = http.DefaultClient.Do(req)
	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusBadRequest || res.StatusCode == http.StatusNotFound {
		return render(
			w, r,
			"pswd-recovery",
			"Please enter the email address you have signed up with!",
		)
	}

	if res.StatusCode == http.StatusInternalServerError {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	return render(w, r, "pswd-recovery", "The link has been sent.")
}

// -------------------------
// new-pswd

func getNewPswdPage(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	return render(w, r, "new-pswd", nil)
}

func handleNewPswd(
	w http.ResponseWriter,
	r *http.Request,
	args *nm.Args,
) bool {
	var token = args.HostPathValues().Get("token")
	var pswd = r.FormValue("new-pswd")

	if pswd == "" {
		return render(w, r, "new-pswd", "The password must not be empty!")
	}

	var nPswd = _NewPassword{Token: token, Password: pswd}

	var buf bytes.Buffer
	var err = json.NewEncoder(&buf).Encode(&nPswd)
	if err != nil {
		handleErr(w, r, http.StatusInternalServerError)
	}

	var res *http.Response
	res, err = http.Post(
		"http://localhost:8001/api/pswd-recovery/new",
		"application/json",
		&buf,
	)

	if err != nil {
		return handleErr(w, r, http.StatusInternalServerError)
	}

	defer res.Body.Close()

	if res.StatusCode == http.StatusForbidden ||
		res.StatusCode == http.StatusNotFound {
		return render(w, r, "new-pswd", "Forbidden!")
	}

	if res.StatusCode == http.StatusUnauthorized {
		return render(
			w, r,
			"pswd-recovery",
			"The link has expired. Please get a new one!",
		)
	}

	if res.StatusCode == http.StatusOK {
		return render(w, r, "login", nil)
	}

	return true
}

// --------------------------------------------------
