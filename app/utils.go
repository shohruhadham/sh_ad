package app

import (
	"net/http"
	"os"
	"sync"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/endpoints"
)

// --------------------------------------------------

type _User struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type _UserProfile struct {
	ReadOnlyEmail bool   `json:"readOnlyEmail"`
	Email         string `json:"email"`
	FullName      string `json:"fullName"`
	Address       string `json:"address"`
	PhoneNumber   string `json:"phoneNumber"`

	Warning string `json:"-"`
}

type _Tokens struct {
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

type _GToken struct {
	AccessToken  string    `json:"accessToken"`
	RefreshToken string    `json:"refreshToken"`
	TokenType    string    `json:"tokenType"`
	ExpTime      time.Time `json:"expTime"`
}

type _NewPassword struct {
	Token    string `json:"token"`
	Password string `json:"password"`
}

// -------------------------

var gOauth2Config *oauth2.Config

func init() {
	gOauth2Config = &oauth2.Config{
		ClientID:     os.Getenv("SH_AD_ID"),
		ClientSecret: os.Getenv("SH_AD_S"),

		// TODO: localhost:8001 must be replaced with real host.
		RedirectURL: "http://localhost:8000/glogin/user",
		Scopes:      []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:    endpoints.Google,
	}
}

var gOauth2State = "SH_AD_ST"

var tokenMap sync.Map

// -------------------------

func setCacheControl(w http.ResponseWriter) {
	w.Header().Set(
		"Cache-Control",
		"no-store, max-age=0, must-revalidate, private, no-cache",
	)
}

// -------------------------

func handleErr(w http.ResponseWriter, r *http.Request, statusCode int) bool {
	var cookie = &http.Cookie{
		Name:    "tk",
		Value:   "",
		Expires: time.Now().AddDate(-99, 0, 0),
	}

	http.SetCookie(w, cookie)

	cookie = &http.Cookie{
		Name:    "rtk",
		Value:   "",
		Expires: time.Now().AddDate(-99, 0, 0),
	}

	http.SetCookie(w, cookie)

	setCacheControl(w)
	w.WriteHeader(statusCode)

	render(w, r, "login", http.StatusText(statusCode))
	return true
}

func handleUnauthenticated(w http.ResponseWriter, r *http.Request) bool {
	var cookie = &http.Cookie{
		Name:    "tk",
		Value:   "",
		Expires: time.Now().AddDate(-99, 0, 0),
	}

	http.SetCookie(w, cookie)

	cookie = &http.Cookie{
		Name:    "rtk",
		Value:   "",
		Expires: time.Now().AddDate(-99, 0, 0),
	}

	http.SetCookie(w, cookie)

	setCacheControl(w)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
	return true
}
