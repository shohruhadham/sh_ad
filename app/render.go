package app

import (
	"html/template"
	"net/http"
)

var tmpl *template.Template

func init() {
	tmpl = template.Must(
		template.ParseFiles(
			"./app/templates/login-page.html",
			"./app/templates/sign-up-page.html",
			"./app/templates/profile-form-page.html",
			"./app/templates/profile-view-page.html",
			"./app/templates/pswd-recovery-page.html",
			"./app/templates/new-pswd-page.html",
		),
	)
}

// --------------------------------------------------

func render(
	w http.ResponseWriter,
	r *http.Request,
	templName string,
	model interface{},
) bool {
	setCacheControl(w)

	var err = tmpl.ExecuteTemplate(w, templName, model)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	return true
}
