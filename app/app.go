package app

import (
	"net/http"

	nm "github.com/shohruhadham/nanomux"
)

// --------------------------------------------------

func New() *nm.Resource {
	var root = nm.NewDormantResource("/")
	root.SetHandlerFor(
		"GET",
		func(w http.ResponseWriter, r *http.Request, args *nm.Args) bool {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return true
		},
	)

	root.SetPathHandlerFor("GET", "login", getLoginPage)
	root.SetPathHandlerFor("POST", "login", handleLogin)

	root.SetPathHandlerFor("GET", "glogin/", handleGLogin)
	root.SetPathHandlerFor("GET", "glogin/user", handleGLoginUser)

	// The handleRenewal must be called for both the GET and POST methods
	// to refresh the access token.
	root.SetPathHandlerFor("GET, POST", "renewal", handleRenewal)

	root.SetPathHandlerFor("GET", "logout", handleLogout)

	root.SetPathHandlerFor("GET", "sign-up", getSignUpPage)
	root.SetPathHandlerFor("POST", "sign-up", handleSignUp)

	root.SetPathHandlerFor("GET", "profile-form", getProfileFormPage)
	root.SetPathHandlerFor("POST", "profile-form", handlePostProfileForm)

	root.SetPathHandlerFor("GET", "profile-view", getProfileViewPage)

	root.SetPathHandlerFor("GET", "pswd-recovery/", getPswdRecoveryPage)
	root.SetPathHandlerFor("POST", "pswd-recovery/", handlePswdRecoveryEmail)

	root.SetPathHandlerFor("GET", "pswd-recovery/{token}", getNewPswdPage)
	root.SetPathHandlerFor("POST", "pswd-recovery/{token}", handleNewPswd)

	return root
}
